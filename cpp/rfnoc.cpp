#include <uhd/device3.hpp>
#include <uhd/exception.hpp>
#include <uhd/rfnoc/block_ctrl.hpp>
#include <uhd/rfnoc/radio_ctrl.hpp>
#include <uhd/rfnoc/null_block_ctrl.hpp>
#include <uhd/rfnoc/ddc_block_ctrl.hpp>
#include <uhd/rfnoc/source_block_ctrl_base.hpp>
#include <uhd/types/tune_request.hpp>
#include <uhd/types/sensors.hpp>
#include <uhd/utils/math.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <complex>
#include <csignal>
#include <fstream>
#include <iostream>
#include <thread>
#include <locale>
#include <string>

#include "jlcxx/jlcxx.hpp"


namespace po = boost::program_options;
using uhd::rfnoc::radio_ctrl;

// Define Ids 
const std::string ID_FH_DETECT = "fhdetect_0";
const std::string ID_FIFO = "0/DmaFIFO_0";
const std::string ID_FFT = "fftTempest_0";
const std::string ID_DDC = "DDC";
const std::string ID_RADIO0 = "0/Radio_0"; //Radio frontend select
uhd::rx_streamer::sptr rx_stream;
boost::shared_ptr<uhd::device3> x310;

void* createRadio(std::string args)
{
	x310 = boost::dynamic_pointer_cast<uhd::device3>(uhd::device::make(args));
	void* x310_ptr = NULL;
	x310_ptr = (void *)&x310;
	return x310_ptr;
}



std::vector<float> getSingleBuffer(void* streamer_ptr, std::vector<float> buff , size_t samps_per_buff)
{
	bool enable_size_map        = false;
	rx_stream = *(uhd::rx_streamer::sptr *)(streamer_ptr);
	unsigned long long num_total_samps = 0;

	uhd::rx_metadata_t md;

	// setup streaming
	uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
	stream_cmd.num_samps  = size_t(0);
	stream_cmd.stream_now = true;
	stream_cmd.time_spec  = uhd::time_spec_t();
	//std::cout << "Issuing stream cmd" << std::endl;
	rx_stream->issue_stream_cmd(stream_cmd);

	// Track time and samps between updating the BW summary
	size_t num_rx_samps = rx_stream->recv(&buff.front(), samps_per_buff/2, md, 3.0, enable_size_map);
	//std::cout << "Getting actual samples :> " << num_rx_samps  << std::endl;
	/*
	std::ofstream outfile;
	std::string file =  "usrp_samples.dat"; //      ("null", "run without writing to file")
	if (not file.empty())  
		outfile.open(file.c_str(), std::ofstream::binary);
	if (outfile.is_open()) 
		outfile.write((const char*)&buff.front(), num_rx_samps * sizeof(float));

	if (outfile.is_open())
		outfile.close();
		*/
	return buff;
}

void* destroyRadio(void* x310_ptr, void* streamer_ptr)
{
	rx_stream = *(uhd::rx_streamer::sptr *)(streamer_ptr);
    rx_stream.reset();
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	x310.reset();
}


void* setup(void* x310_ptr, double freq, double rate,double gain,std::string ant,std::string streamargs,size_t radio_chan)
{ 
	// Init all variables	
	std::string format, subdev, ref, wirefmt, block_id, block_args;
	size_t spp;
	double bw;

	/////////////////////////////////////////////////////////////////////////
	/////////// Parameters //////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	format                      = "fc32";
	bw                          = rate; //analog frontend filter bandwidth in Hz, !!! Will be overwritten in after DDC rate
	ref                         = "internal"; //reference source (internal, external, mimo)")


	// RFNoc parameters
	spp = 1024; //sample per packet    
	radio_chan = 0;//"radio channel to select inside ONE frontend
	format = "fc32";

	// X310 object 
	x310 = *(boost::shared_ptr<uhd::device3> *)(x310_ptr);

	// IP discovering
	auto rx_radio_ctrl  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);

	/////////////////////////////////////////////////////////////////////////
	/////////// Configure Rx radio & DDC ////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	// rx_radio_ctrl->set_args(radio_args); 
	rx_radio_ctrl->set_clock_source(ref);

	// set the center frequency
	std::cout << boost::format("RX Freq \t Target: %f MHz \t-\t") % (freq / 1e6); 
	freq = rx_radio_ctrl->set_rx_frequency(freq, 0);
	//rx_radio_ctrl->set_rx_lo_freq(rate,"",radio_chan); //remove dc offset
	//rx_radio_ctrl->set_arg = ("mode_n", "integer");   
	std::cout << boost::format("Real: %f MHz") % (freq / 1e6) << std::endl;

	// set the rf gain
	std::cout << boost::format("RX Gain \t Target: %f dB \t\t-\t") % gain;
	gain = rx_radio_ctrl->set_rx_gain(gain, radio_chan);
	std::cout << boost::format("Real: %f dB") % gain << std::endl;

	// // rx_radio_ctrl->set_rate(rate); // Cette commande est un troll, la radio ne peut que sortir 200 MSPS

	// set the antenna
	rx_radio_ctrl->set_rx_antenna(ant, radio_chan);

	// set rate
	//if (not x310->has_block(ID_DDC)) {std::cout << "No DDC in device " << ID_DDC << std::endl; return EXIT_FAILURE; }
	auto ddc_ctrl       = x310->get_block_ctrl<uhd::rfnoc::ddc_block_ctrl>(ID_DDC);        
	std::cout << boost::format("RX Rate \t Target: %f MHz \t-\t") % (rate / 1e6); 
	ddc_ctrl->set_arg("input_rate", 200e6,0); //change the 0 if multiple DDC
	ddc_ctrl->set_arg("output_rate", rate,0);
	ddc_ctrl->set_arg("fullscale", 1.0,0);
	ddc_ctrl->set_arg("freq", 0.0,0);    
	rate = ddc_ctrl->get_args().cast<float>("output_rate",-1e6);    
	std::cout << boost::format("Real: %f Msps") % (rate / 1e6) << std::endl;

	// set the IF filter bandwidth
	bw = rate;
	std::cout << boost::format("RX Bandwidth \t Target: %f MHz \t\t") % (bw/0e6); 
	bw = rx_radio_ctrl->set_rx_bandwidth(bw, radio_chan); 
	std::cout << boost::format("Real: %f MHz")% (bw/1e6) << std::endl;

	// FFT 
	auto fft = x310->get_block_ctrl<uhd::rfnoc::block_ctrl_base>(ID_FFT);       
	auto detecte = x310->get_block_ctrl<uhd::rfnoc::block_ctrl_base>(ID_FH_DETECT);       
	std::cout << "level is 0" << std::endl;
	detecte->set_arg("channel_level",0,0);
	detecte->set_arg("spp",spp,0);

	// IP connection 
	auto fifo_ctrl      = x310->get_block_ctrl<uhd::rfnoc::block_ctrl_base>(ID_FIFO);        
	uhd::device_addr_t streamer_args(streamargs);
	uhd::rfnoc::graph::sptr rx_graph = x310->create_graph("tempest");
	x310->clear();
	rx_graph->connect(rx_radio_ctrl->get_block_id() , radio_chan, ddc_ctrl->get_block_id(), 0, spp);
	rx_graph->connect(ddc_ctrl->get_block_id() , 0, fft->get_block_id(), 0, spp);
	rx_graph->connect(fft->get_block_id(), 0, detecte->get_block_id(), 0, spp);
	// rx_graph->connect(ddc_ctrl->get_block_id(), 0, fh_detect_ctrl->get_block_id(), 0, spp);
	rx_graph->connect(detecte->get_block_id(), 0, fifo_ctrl->get_block_id(), 0, spp);
	streamer_args["block_id"] = fifo_ctrl->get_block_id().to_string();


	// create a receive streamer
	std::cout << "Samples per packet: " << spp << std::endl;
	uhd::stream_args_t stream_args(format, "sc16"); // We should read the wire format from the blocks
	stream_args.args        = streamer_args;
	stream_args.args["spp"] = boost::lexical_cast<std::string>(spp);
	std::cout << "Using streamer args: " << stream_args.args.to_string() << std::endl;
	rx_stream = x310->get_rx_stream(stream_args);    

	void *stream_ptr     = NULL;
	stream_ptr = (void*)&rx_stream;


	return stream_ptr;
}

double getCarrierFreq(void* x310_ptr,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto radio  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);
	// Return carrier frequency
	return radio->get_rx_frequency(chan);
}
double setCarrierFreq(void* x310_ptr,const double carrierFreq,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto radio  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);
	// Return carrier frequency
	return radio->set_rx_frequency(carrierFreq,chan);
}


double getSamplingRate(void* x310_ptr,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto ddc_ctrl       = x310->get_block_ctrl<uhd::rfnoc::ddc_block_ctrl>(ID_DDC);        
	return ddc_ctrl->get_args().cast<float>("output_rate",-1e6);    
}
double setSamplingRate(void* x310_ptr,const double rate,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto ddc_ctrl       = x310->get_block_ctrl<uhd::rfnoc::ddc_block_ctrl>(ID_DDC);        
	ddc_ctrl->set_arg("output_rate", rate,chan);
	// We need to set also the IF frequency 
	auto rx_radio_ctrl  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);
	double bw = rx_radio_ctrl->set_rx_bandwidth(rate, chan); 
	// Return carrier frequency
	return ddc_ctrl->get_args().cast<float>("output_rate",-1e6);    
}

double getGain(void* x310_ptr,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto radio  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);
	// Return carrier frequency
	return radio->get_rx_gain(chan);
}
double setGain(void* x310_ptr,const double gain,const size_t chan)
{
	// Radio casting 
	x310 = *(boost::shared_ptr<uhd::device3> *)x310_ptr;
	// Instantiate radio controller to get frequency 
	auto radio  = x310->get_block_ctrl<radio_ctrl>(ID_RADIO0);
	// Return carrier frequency
	return radio->set_rx_gain(gain,chan);
}

JLCXX_MODULE define_julia_module(jlcxx::Module& mod)
{
	// Radio configuration 
	mod.method("createRadio", &createRadio);
	mod.method("setup", &setup);
	mod.method("getSingleBuffer", &getSingleBuffer);
	mod.method("destroyRadio", &destroyRadio);

	// Carrier frequency managment
	mod.method("getCarrierFreq", &getCarrierFreq);
	mod.method("setCarrierFreq", &setCarrierFreq);
	// Sampling Rate managment 
	mod.method("getSamplingRate", &getSamplingRate);
	mod.method("setSamplingRate", &setSamplingRate);
	// Carrier frequency managment
	mod.method("getGain", &getGain);
	mod.method("setGain", &setGain);

}


