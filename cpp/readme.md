# Cpp part 


In this folder there are all the sources associated to Julia <-> RFNoC link 
- The file has to be compiled using the path of CxxWrap 
- -Be sure to source the uhd path correctly beforehand
- The CxxWrap path can be obtained in Julia


    # In the project, after activating it 
    julia> using CxxWrap 
    julia> CxxWrap.prefix_path()
    /home/rose/.julia/artifacts/e2cb072a27ace96a6d021e472a270730c34893e0

And then compile using 

     cmake -DCMAKE_PREFIX_PATH=/home/rose/.julia/artifacts/e2cb072a27ace96a6d021e472a270730c34893e0 ..



Then `make` and `make install`
It will lead to a .so file that can be used in the main module
