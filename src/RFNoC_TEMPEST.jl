module RFNoC_TEMPEST

# Write your package code here.

# First we load the CxxWrap module to have all the embedded methods 
module RFNoc_CXX
using CxxWrap
    @wrapmodule(joinpath("cpp/build/", "librfnoc.so"))
    function __init__()
        @initcxx
    end
end 

# ----------------------------------------------------
# --- Print function 
# ---------------------------------------------------- 
# To print fancy message with different colors with Tx and Rx
function customPrint(str,handler;style...)
    msglines = split(chomp(str), '\n')
    printstyled("┌",handler,": ";style...)
    println(msglines[1])
    for i in 2:length(msglines)
        (i == length(msglines)) ? symb="└ " : symb = "|";
        printstyled(symb;style...);
        println(msglines[i]);
    end
end
# define macro for printing Rx info
macro inforx(str)
    quote
        customPrint($(esc(str)),"Rx";bold=true,color=:light_green)
    end
end
# define macro for printing Rx warning 
macro warnrx(str)
    quote
        customPrint($(esc(str)),"Rx Warning";bold=true,color=:light_yellow)
    end
end
# define macro for printing Tx info
macro infotx(str)
    quote
        customPrint($(esc(str)),"Tx";bold=true,color=:light_blue)
    end
end
# define macro for printing Tx warning 
macro warntx(str)
    quote
        customPrint($(esc(str)),"Tx Warning";bold=true,color=:light_yellow)
    end
end

# ----------------------------------------------------
# --- Required module 
# ---------------------------------------------------- 
using CxxWrap

# ----------------------------------------------------
# --- Exporting functions 
# ---------------------------------------------------- 
export openUHD 
export recv
export close


export updateCarrierFreq;
export updateSamplingRate;
export updateGain;

# ----------------------------------------------------
# --- Define runtime structure 
# ---------------------------------------------------- 
struct UHDRxWrapper
    streamer_ptr::Ptr{Nothing};
    chan::UInt;
end
mutable struct UHDRx 
    uhd::UHDRxWrapper;
    carrierFreq::Float64;
	samplingRate::Float64;
	gain::Union{Int,Float64}; 
	antenna::String;
	packetSize::Csize_t;
	released::Int;
end
mutable struct UHDBinding 
    usrp_ptr::Ptr{Nothing};
	rx::UHDRx;
end



# ----------------------------------------------------
# --- Define main call functions
# ---------------------------------------------------- 
""" 
Init the core parameter of the radio in Tx or in Rx mode and initiate RF parameters 

# --- Syntax 

openUHD(mode,sysImage,carrierFreq,samplingRate,txGain,antenna="RX2")
# --- Input parameters 
- mode 			: String to open radio in "Tx" (transmitter) or in "Rx" (receive) mode
- carrierFreq	: Desired Carrier frequency [Union{Int,Float64}] 
- samplingRate	: Desired bandwidth [Union{Int,Float64}] 
- gain		: Desired Gain [Union{Int,Float64}] 
- antenna		: Desired Antenna alias [String]
Keywords=
- args	  : String with the additionnal load parameters (for instance, path to the FPHGA image) [String]
# --- Output parameters 
- uhd		  	: UHD object [UHDBinding]
""" 
function openUHD(carrierFreq, samplingRate, gain, antenna = "RX2";args="",chan=UInt(0)::UInt)
    # Create the radio device 
    usrp_ptr  =  RFNoc_CXX.createRadio(args);
    # Init all the stage and create the streamer 
    # TODO :> This function should be split into 3 functions [setup DDC, create graph and init streamer]
    streamer_ptr = RFNoc_CXX.setup(usrp_ptr,carrierFreq, samplingRate, gain, antenna,"",chan);
    # --- Get all parameters of USRP 
    effectiveFreq = RFNoc_CXX.getCarrierFreq(usrp_ptr,chan);
    effectiveSamplingRate =  RFNoc_CXX.getSamplingRate(usrp_ptr,chan);
    effectiveGain =RFNoc_CXX.getGain(usrp_ptr,chan); 
    # Create the UHD Object Wrapper
    wrapper = UHDRxWrapper(streamer_ptr,chan);
    # Encapsulate it into Rx object 
    rx = UHDRx(
        wrapper,
        effectiveFreq,
        effectiveSamplingRate,
        effectiveGain,
        antenna,
        100, #FIXME :> Get this
        0
    )
    # Create the complete radio object 
    usrp = UHDBinding(usrp_ptr,rx);
    # Update the object based on real register 

end

""" 
Close the USRP device (Rx or Tx mode) and release all associated objects

# --- Syntax 

close(uhd)
# --- Input parameters 
- uhd	: UHD object [UHDBinding]
# --- Output parameters 
- []
"""
function Base.close(uhdBinding::UHDBinding)
    # Call the destructor 
    RFNoc_CXX.destroyRadio(uhdBinding.usrp_ptr,uhdBinding.rx.uhd.streamer_ptr);
    uhdBinding = nothing;
end

""" 
Get a single buffer from the USRP device, and create all the necessary ressources

# --- Syntax 

sig	  = recv(radio,nbSamples)
# --- Input parameters 
- radio	  : UHD object [UHDRx]
- nbSamples : Desired number of samples [Int]
# --- Output parameters 
- sig	  : baseband signal from radio [Array{Complex{CFloat}},radio.packetSize]
"""
function recv(radio::UHDBinding,nbSamples=0);
    buffer = StdVector(zeros(Cfloat, nbSamples));
    return buff = reinterpret(Complex{Cfloat}, collect(RFNoc_CXX.getSingleBuffer(radio.rx.uhd.streamer_ptr, buffer, nbSamples)));
end


function getCarrierFreq(usrp::UHDBinding);
    return RFNoc_CXX.getCarrierFreq(usrp.usrp_ptr,usrp.rx.uhd.chan);
end
function updateCarrierFreq(usrp::UHDBinding,carrierFreq::Cdouble);
    # Tune the frequency 
    effectiveFreq =  RFNoc_CXX.setCarrierFreq(usrp.usrp_ptr,carrierFreq,usrp.rx.uhd.chan);
    # Raise a warning if desired freq is not tuned properly
    if effectiveFreq != carrierFreq 
        @warnrx "Effective carrier frequency is $(effectiveFreq/1e6) MHz and not $(carrierFreq/1e6) MHz\n" 
    end
    # Update USRP field 
    usrp.rx.carrierFreq = effectiveFreq;
    return effectiveFreq;
end




function getSamplingRate(usrp::UHDBinding);
    return RFNoc_CXX.getSamplingRate(usrp.usrp_ptr,usrp.rx.uhd.chan);
end
function updateSamplingRate(usrp::UHDBinding,samplingRate::Cdouble);
    # Tune the frequency 
    effectiveRate =  RFNoc_CXX.setSamplingRate(usrp.usrp_ptr,samplingRate,usrp.rx.uhd.chan);
    # Raise a warning if desired freq is not tuned properly
    if effectiveRate != samplingRate 
        @warnrx "Effective carrier frequency is $(effectiveRate/1e6) MHz and not $(samplingRate/1e6) MHz\n" 
    end
    # Update USRP field 
    usrp.rx.samplingRate = effectiveRate;
    return effectiveRate;
end




function getGain(usrp::UHDBinding);
    return RFNoc_CXX.getGain(usrp.usrp_ptr,usrp.rx.uhd.chan);
end
function updateGain(usrp::UHDBinding,gain);
    # Tune the frequency 
    effectiveGain =  RFNoc_CXX.setGain(usrp.usrp_ptr,Cdouble(gain),usrp.rx.uhd.chan);
    # Raise a warning if desired freq is not tuned properly
    if effectiveGain != gain 
        @warnrx "Effective carrier frequency is $(effectiveGain/1e6) MHz and not $(gain/1e6) MHz\n" 
    end
    # Update USRP field 
    usrp.rx.gain = effectiveGain;
    return effectiveGain;
end




end
