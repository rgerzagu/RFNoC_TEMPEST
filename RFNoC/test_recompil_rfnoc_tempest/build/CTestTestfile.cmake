# CMake generated Testfile for 
# Source directory: /home/rose/Documents/Work/rfnoc_corentin/src/test_recompil_rfnoc_tempest
# Build directory: /home/rose/Documents/Work/rfnoc_corentin/src/test_recompil_rfnoc_tempest/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include/tempest")
subdirs("lib")
subdirs("swig")
subdirs("python")
subdirs("grc")
subdirs("apps")
subdirs("docs")
subdirs("rfnoc/blocks")
subdirs("rfnoc/testbenches")
