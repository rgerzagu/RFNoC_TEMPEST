var files =
[
    [ "api.h", "api_8h.html", "api_8h" ],
    [ "fftTempest.h", "fftTempest_8h.html", [
      [ "fftTempest", "classgr_1_1tempest_1_1fftTempest.html", "classgr_1_1tempest_1_1fftTempest" ]
    ] ],
    [ "fftTempest_impl.h", "fftTempest__impl_8h.html", [
      [ "fftTempest_impl", "classgr_1_1tempest_1_1fftTempest__impl.html", "classgr_1_1tempest_1_1fftTempest__impl" ]
    ] ],
    [ "fhdetect.h", "fhdetect_8h.html", [
      [ "fhdetect", "classgr_1_1tempest_1_1fhdetect.html", "classgr_1_1tempest_1_1fhdetect" ]
    ] ],
    [ "fhdetect_impl.h", "fhdetect__impl_8h.html", [
      [ "fhdetect_impl", "classgr_1_1tempest_1_1fhdetect__impl.html", "classgr_1_1tempest_1_1fhdetect__impl" ]
    ] ],
    [ "gain.h", "gain_8h.html", [
      [ "gain", "classgr_1_1tempest_1_1gain.html", "classgr_1_1tempest_1_1gain" ]
    ] ],
    [ "gain_impl.h", "gain__impl_8h.html", [
      [ "gain_impl", "classgr_1_1tempest_1_1gain__impl.html", "classgr_1_1tempest_1_1gain__impl" ]
    ] ],
    [ "sdft.h", "sdft_8h.html", [
      [ "sdft", "classgr_1_1tempest_1_1sdft.html", "classgr_1_1tempest_1_1sdft" ]
    ] ],
    [ "sdft_impl.h", "sdft__impl_8h.html", [
      [ "sdft_impl", "classgr_1_1tempest_1_1sdft__impl.html", "classgr_1_1tempest_1_1sdft__impl" ]
    ] ]
];