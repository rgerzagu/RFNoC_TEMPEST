# CMake generated Testfile for 
# Source directory: /home/rose/Documents/Work/rfnoc_corentin/src/test_recompil_rfnoc_tempest/rfnoc/testbenches
# Build directory: /home/rose/Documents/Work/rfnoc_corentin/src/test_recompil_rfnoc_tempest/build/rfnoc/testbenches
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("noc_block_gain_tb")
subdirs("noc_block_fhdetect_tb")
subdirs("noc_block_sdft_tb")
subdirs("noc_block_fftTempest_tb")
