
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::tempest::fftTempest "<+description of block+>"

%feature("docstring") gr::tempest::fftTempest::make "Return a shared_ptr to a new instance of tempest::fftTempest.

To avoid accidental use of raw pointers, tempest::fftTempest's constructor is in a private implementation class. tempest::fftTempest::make is the public interface for creating new instances.

Params: (dev, tx_stream_args, rx_stream_args, block_select, device_select, enable_eob_on_stop)"

%feature("docstring") gr::tempest::fhdetect "<+description of block+>"

%feature("docstring") gr::tempest::fhdetect::make "Return a shared_ptr to a new instance of tempest::fhdetect.

To avoid accidental use of raw pointers, tempest::fhdetect's constructor is in a private implementation class. tempest::fhdetect::make is the public interface for creating new instances.

Params: (dev, tx_stream_args, rx_stream_args, block_select, device_select, enable_eob_on_stop)"

%feature("docstring") gr::tempest::gain "<+description of block+>"

%feature("docstring") gr::tempest::gain::make "Return a shared_ptr to a new instance of tempest::gain.

To avoid accidental use of raw pointers, tempest::gain's constructor is in a private implementation class. tempest::gain::make is the public interface for creating new instances.

Params: (dev, tx_stream_args, rx_stream_args, block_select, device_select, enable_eob_on_stop)"

%feature("docstring") gr::tempest::sdft "<+description of block+>"

%feature("docstring") gr::tempest::sdft::make "Return a shared_ptr to a new instance of tempest::sdft.

To avoid accidental use of raw pointers, tempest::sdft's constructor is in a private implementation class. tempest::sdft::make is the public interface for creating new instances.

Params: (dev, tx_stream_args, rx_stream_args, block_select, device_select, enable_eob_on_stop)"