/* -*- c++ -*- */

#define TEMPEST_API
#define ETTUS_API

%include "gnuradio.i"/*			*/// the common stuff

//load generated python docstrings
%include "tempest_swig_doc.i"
//Header from gr-ettus
%include "ettus/device3.h"
%include "ettus/rfnoc_block.h"
%include "ettus/rfnoc_block_impl.h"

%{
#include "ettus/device3.h"
#include "ettus/rfnoc_block_impl.h"
#include "tempest/gain.h"
#include "tempest/fhdetect.h"
#include "tempest/sdft.h"
#include "tempest/fftTempest.h"
%}

%include "tempest/gain.h"
GR_SWIG_BLOCK_MAGIC2(tempest, gain);
%include "tempest/fhdetect.h"
GR_SWIG_BLOCK_MAGIC2(tempest, fhdetect);

%include "tempest/sdft.h"
GR_SWIG_BLOCK_MAGIC2(tempest, sdft);
%include "tempest/fftTempest.h"
GR_SWIG_BLOCK_MAGIC2(tempest, fftTempest);
