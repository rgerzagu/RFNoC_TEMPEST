//the last bin witch are not multiple of IN/OUT are discarted

module slidingMeans #(
  parameter MAX_WINDOW_WIDTH = 9
)(
  input clk, input reset,
  input  [15:0] i_tdata, input i_tlast, input i_tvalid, output i_tready,
  input  [7:0] i_mean_bin,
  output [31:0] o_tdata, output o_tlast, output o_tvalid, input o_tready
);

  //reg [8:0]  bin_counter;  
  reg [25:0] temp_data; 
  reg [25:0] temp_sum; 
  reg lasted, first;


  //data shift register
  reg  [15:0] sample_shift_reg[0:MAX_WINDOW_WIDTH-1];
  integer n;
  integer k;
  integer v;
  initial begin
    for (n = 0; n < MAX_WINDOW_WIDTH-1; n = n + 1) begin
      sample_shift_reg[n] <= 0;
    end
    temp_sum<=0;
  end

  generate
  always @(posedge clk) begin    
    if (i_tvalid & i_tready & !(i_tlast | reset)) begin
      for (k = 1; k < MAX_WINDOW_WIDTH-1; k = k + 1) begin
        sample_shift_reg[k] <= sample_shift_reg[k-1];
      end
      sample_shift_reg[0] <= i_tdata;       
      temp_sum <= temp_sum + i_tdata[15:4];
    end //if (i_tvalid & i_tready) 
    else begin
      temp_sum<=0;
      for (v = 0; v < MAX_WINDOW_WIDTH-1; v = v + 1) begin
        sample_shift_reg[v] <= 0;  
      end
    end
  end
  endgenerate

  always @(posedge clk) begin
    if(i_tlast | reset)begin
      lasted <=1;     
      
    end

    if (i_tvalid & i_tready & lasted) begin
      lasted <=0;
      first <=1;
      temp_data <=0;
      //bin_counter <=0;
    end else if (first) begin
      first <=0;
    end

    if (i_mean_bin == 2) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1];
    end else if (i_mean_bin == 3) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2];
    end else if (i_mean_bin == 4) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3];
    end else if (i_mean_bin == 5) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4];
    end else if (i_mean_bin == 6) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4] + sample_shift_reg[5];
    end else if (i_mean_bin == 7) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4] + sample_shift_reg[5] + sample_shift_reg[6];
    end else if (i_mean_bin == 8) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4] + sample_shift_reg[5] + sample_shift_reg[6] + sample_shift_reg[7];
    end else if (i_mean_bin == 9) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4] + sample_shift_reg[5] + sample_shift_reg[6] + sample_shift_reg[7] + sample_shift_reg[8];
    end else if (i_mean_bin == 10) begin
      temp_data <= sample_shift_reg[0] + sample_shift_reg[1] + sample_shift_reg[2] + sample_shift_reg[3] + sample_shift_reg[4] + sample_shift_reg[5] + sample_shift_reg[6] + sample_shift_reg[7] + sample_shift_reg[8] + sample_shift_reg[9];
    end else begin
      temp_data <= sample_shift_reg[0];
    end      
    //bin_counter <= bin_counter+1;    
  end //always @(posedge clk)


//seems useless
 axi_fifo #(.WIDTH(33)) axi_fifo (
    .clk(clk), .reset(reset), .clear(),
    .i_tdata({i_tlast,{temp_data[15:0],temp_sum[15:0]}}), .i_tvalid(i_tvalid), .i_tready(i_tready),
    //.i_tdata({i_tlast,{temp_data[17:2],sample_shift_reg[0]}}), .i_tvalid(i_tvalid), .i_tready(i_tready),    
    .o_tdata({o_tlast,o_tdata}), .o_tvalid(o_tvalid), .o_tready(o_tready),
    .occupied(), .space());

  //assign o_tvalid = i_tvalid;
  //assign o_tlast  = i_tlast;
  //assign o_tdata = {temp_data[15:0],temp_sum[15:0]};//pas de decalage necessaire
  //assign i_tready = o_tready;


endmodule