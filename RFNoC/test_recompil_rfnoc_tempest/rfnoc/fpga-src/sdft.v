//first channel is 1, 0 mean no channel detected

module sdft #(
  parameter BIN_N = 100
)(
  input clk, input reset, input clear,
  input  [31:0] i_tdata, input i_tlast, input i_tvalid, output i_tready,
  output [31:0] o_tdata[0:BIN_N-1], output o_tlast, output o_tvalid, input o_tready
);

reg done, ready;

reg [$clog2(BIN_N)+1:0] x_index; //index of the next item in the buffer to be used. Equivalently, the number of samples that have been seen so far modulo BIN_N

reg [31:0] twiddle [0:BIN_N-1];     //Twiddle factors for the update algorithm
reg [31:0] S [0:BIN_N-1];           //Unwindowed frequency domain values
reg [15:0] damping_factor;                  //A damping factor introduced into the recursive DFT algorithm to guarantee stability
reg [31:0] dft [0:BIN_N-1];  //Output frequency domain values
reg [31:0] old_i;
reg [63:0] temp1 [0:BIN_N-1];
reg [31:0] temp2 [0:BIN_N-1];
reg [31:0] temp3 [0:BIN_N-1];
reg [31:0] temp4 [0:BIN_N-1];

wire [31:0] delta;
wire temp1_tready, temp1_tlast, temp1_tvalid;
wire temp2_tready, temp2_tlast, temp2_tvalid;
wire temp3_tready, temp3_tlast, temp3_tvalid;
wire temp4_tready, temp4_tlast, temp4_tvalid;

// always @* begin
//   delta[15:0] <= {i_tdata[15:0] - old_i[15:0]};
//   delta[31:16] <= {i_tdata[31:16] - old_i[31:16]};
// end

//   always @(posedge clk) begin
//     old_i <= i_tdata;
//   end

//   // for (k = 0;1;nbBin-1)
//   //     factor = (2.0 * pi) * k / nbBin
//   //     stru.twiddle[k+1] = exp(j * factor)
//   //   end



// generate
//   integer k;
//   always @(posedge clk) begin
//     old_i <= i_tdata;

//     for (k = 1; k < BIN_N ; k = k +1) begin

//       //damping_factor * S[k] + delta) real
//       cmul damping_factorXS ( .clk(clk), .reset(reset),
//         .a_tdata(damping_factor), .a_tlast(i_tlasti_tlast),  .a_tvalid(i_tvalid), .a_tready(i_tready),
//         .b_tdata(S[k]),           .b_tlast(i_tlast),  .b_tvalid(i_tvalid), .b_tready(1'b1),
//         .o_tdata(temp1[k]),       .o_tlast(temp1_tlast),  .o_tvalid(temp1_tvalid), .o_tready(temp1_tready));

//       axi_round_and_clip_complex #(.WIDTH_IN(32), .WIDTH_OUT(16), .CLIP_BITS(3), .FIFOSIZE(1)) roundTemp1
//         (.clk(clk), .reset(reset),
//         .i_tdata(temp1[k]), .i_tlast(temp1_tlast), .i_tvalid(temp1_tvalid), .i_tready(temp1_tready),
//         .o_tdata(temp2[k]), .o_tlast(temp2_tlast), .o_tvalid(temp2_tvalid), .o_tready(temp2_tready));

//       cadd #(.WIDTH(16)) temp2Pdelta
//         (.clk(clk), .reset(reset),
//         .a_tdata(temp2[k]), .a_tlast(temp2_tlast),  .a_tvalid(temp2_tvalid), .a_tready(temp2_tready),
//         .b_tdata(delta),    .b_tlast(temp2_tlast),  .b_tvalid(temp2_tvalid), .b_tready(1'b1),
//         .o_tdata(temp3[k]), .o_tlast(temp3_tlast),  .o_tvalid(temp3_tvalid), .o_tready(temp3_tready));


//       //twiddle[k] * temp real
//       cmul damping_factorXS ( .clk(clk), .reset(reset),
//         .a_tdata(temp3), .a_tlast(i_tlasti_tlast),  .a_tvalid(i_tvalid), .a_tready(i_tready),
//         .b_tdata(S[k]),           .b_tlast(i_tlast),  .b_tvalid(i_tvalid), .b_tready(1'b1),
//         .o_tdata(temp4[k]),       .o_tlast(temp4_tlast),  .o_tvalid(temp4_tvalid), .o_tready(temp4_tready));

//       axi_round_and_clip_complex #(.WIDTH_IN(32), .WIDTH_OUT(16), .CLIP_BITS(3), .FIFOSIZE(1)) roundTemp2
//         (.clk(clk), .reset(reset),
//         .i_tdata(temp4[k]), .i_tlast(temp4_tlast), .i_tvalid(temp4_tvalid), .i_tready(temp4_tready),
//         .o_tdata([k]), .o_tlast(), .o_tvalid(), .o_tready(o_tready));
//     end //end for

//     o_tvalid = o_tvalid[0] & o_tvalid[1] .....
//   end //end allways
// endgenerate



endmodule