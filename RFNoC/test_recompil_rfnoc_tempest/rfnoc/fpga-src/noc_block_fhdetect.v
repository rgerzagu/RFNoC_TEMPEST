
`include "maxi_array.v"



module noc_block_fhdetect #(
  parameter NOC_ID = 64'h5E45_F043_C7EC_4308,
  parameter STR_SINK_FIFOSIZE = 11)
(
  input bus_clk, input bus_rst,
  input ce_clk, input ce_rst,
  input  [63:0] i_tdata, input  i_tlast, input  i_tvalid, output i_tready,
  output [63:0] o_tdata, output o_tlast, output o_tvalid, input  o_tready,
  output [63:0] debug
);

  ////////////////////////////////////////////////////////////
  //
  // RFNoC Shell
  //
  ////////////////////////////////////////////////////////////
  wire [31:0] set_data;
  wire [7:0]  set_addr;
  wire        set_stb;
  reg  [63:0] rb_data;
  wire [7:0]  rb_addr;

  wire [63:0] cmdout_tdata, ackin_tdata;
  wire        cmdout_tlast, cmdout_tvalid, cmdout_tready, ackin_tlast, ackin_tvalid, ackin_tready;

  wire [63:0] str_sink_tdata, str_src_tdata;
  wire        str_sink_tlast, str_sink_tvalid, str_sink_tready, str_src_tlast, str_src_tvalid, str_src_tready;

  wire [15:0] src_sid;
  wire [15:0] next_dst_sid, resp_out_dst_sid;
  wire [15:0] resp_in_dst_sid;

  wire        clear_tx_seqnum;

  noc_shell #(
    .NOC_ID(NOC_ID),
    .STR_SINK_FIFOSIZE(STR_SINK_FIFOSIZE))
  noc_shell (
    .bus_clk(bus_clk), .bus_rst(bus_rst),
    .i_tdata(i_tdata), .i_tlast(i_tlast), .i_tvalid(i_tvalid), .i_tready(i_tready),
    .o_tdata(o_tdata), .o_tlast(o_tlast), .o_tvalid(o_tvalid), .o_tready(o_tready),
    // Computer Engine Clock Domain
    .clk(ce_clk), .reset(ce_rst),
    // Control Sink
    .set_data(set_data), .set_addr(set_addr), .set_stb(set_stb), .set_time(), .set_has_time(),
    .rb_stb(1'b1), .rb_data(rb_data), .rb_addr(rb_addr),
    // Control Source
    .cmdout_tdata(cmdout_tdata), .cmdout_tlast(cmdout_tlast), .cmdout_tvalid(cmdout_tvalid), .cmdout_tready(cmdout_tready),
    .ackin_tdata(ackin_tdata), .ackin_tlast(ackin_tlast), .ackin_tvalid(ackin_tvalid), .ackin_tready(ackin_tready),
    // Stream Sink
    .str_sink_tdata(str_sink_tdata), .str_sink_tlast(str_sink_tlast), .str_sink_tvalid(str_sink_tvalid), .str_sink_tready(str_sink_tready),
    // Stream Source
    .str_src_tdata(str_src_tdata), .str_src_tlast(str_src_tlast), .str_src_tvalid(str_src_tvalid), .str_src_tready(str_src_tready),
    // Stream IDs set by host
    .src_sid(src_sid),                   // SID of this block
    .next_dst_sid(next_dst_sid),         // Next destination SID
    .resp_in_dst_sid(resp_in_dst_sid),   // Response destination SID for input stream responses / errors
    .resp_out_dst_sid(resp_out_dst_sid), // Response destination SID for output stream responses / errors
    // Misc
    .vita_time('d0), .clear_tx_seqnum(clear_tx_seqnum),
    .debug(debug));

  ////////////////////////////////////////////////////////////
  //
  // AXI Wrapper
  // Convert RFNoC Shell interface into AXI stream interface
  //
  ////////////////////////////////////////////////////////////
  wire [31:0] m_axis_data_tdata;
  wire        m_axis_data_tlast;
  wire        m_axis_data_tvalid;
  wire        m_axis_data_tready;
  wire [127:0] m_axis_data_tuser;

  wire [31:0] s_axis_data_tdata;
  wire        s_axis_data_tlast;
  wire        s_axis_data_tvalid;
  wire        s_axis_data_tready;
  wire [127:0] s_axis_data_tuser;
  
  wire [31:0] mag_sq_i_tdata, mag_sq_o_tdata;
  wire        mag_sq_i_tlast, mag_sq_o_tlast;
  wire        mag_sq_i_tvalid, mag_sq_o_tvalid;
  wire        mag_sq_i_tready, mag_sq_o_tready;
  
  wire [31:0] mag_round_i_tdata, mag_round_o_tdata;
  wire        mag_round_i_tlast, mag_round_o_tlast;
  wire        mag_round_i_tvalid, mag_round_o_tvalid;
  wire        mag_round_i_tready, mag_round_o_tready;

  wire [31:0] max_i_tdata; 
  wire [31:0] max_o_tdata;
  wire        max_i_tlast, max_o_tlast;
  wire        max_i_tvalid, max_o_tvalid;
  wire        max_i_tready, max_o_tready;



  wire [31:0] keep_n_i_tdata, keep_n_o_tdata;
  wire        keep_n_i_tlast, keep_n_o_tlast;
  wire        keep_n_i_tvalid, keep_n_o_tvalid;
  wire        keep_n_i_tready, keep_n_o_tready;  

  axi_wrapper #(
    .SIMPLE_MODE(0))//to 0
  inst_axi_wrapper (
    .clk(ce_clk), .reset(ce_rst),
    .bus_clk(bus_clk), .bus_rst(bus_rst),
    .clear_tx_seqnum(clear_tx_seqnum),
    .next_dst(next_dst_sid),
    .set_stb(set_stb), .set_addr(set_addr), .set_data(set_data),
    .i_tdata(str_sink_tdata), .i_tlast(str_sink_tlast), .i_tvalid(str_sink_tvalid), .i_tready(str_sink_tready),
    .o_tdata(str_src_tdata), .o_tlast(str_src_tlast), .o_tvalid(str_src_tvalid), .o_tready(str_src_tready),
    .m_axis_data_tdata(m_axis_data_tdata),
    .m_axis_data_tlast(m_axis_data_tlast),
    .m_axis_data_tvalid(m_axis_data_tvalid),
    .m_axis_data_tready(m_axis_data_tready),
    .m_axis_data_tuser(m_axis_data_tuser),
    .s_axis_data_tdata(s_axis_data_tdata),
    .s_axis_data_tlast(s_axis_data_tlast),
    .s_axis_data_tvalid(s_axis_data_tvalid),
    .s_axis_data_tready(s_axis_data_tready),
    .s_axis_data_tuser(s_axis_data_tuser),
    .m_axis_config_tdata(),
    .m_axis_config_tlast(),
    .m_axis_config_tvalid(),
    .m_axis_config_tready(),
    .m_axis_pkt_len_tdata(),
    .m_axis_pkt_len_tvalid(),
    .m_axis_pkt_len_tready());

  ////////////////////////////////////////////////////////////
  //
  // User code
  //
  ////////////////////////////////////////////////////////////
  localparam SR_USER_REG_BASE = 128;
  localparam MAX_FFT = 1024;

  // Control Source Unused
  assign cmdout_tdata  = 64'd0;
  assign cmdout_tlast  = 1'b0;
  assign cmdout_tvalid = 1'b0;
  assign ackin_tready  = 1'b1;

////////////////////////////////////////////////////////////
//
// Settings registers
//
////////////////////////////////////////////////////////////
  // Settings registers
  //
  // - The settings register bus is a simple strobed interface.
  // - Transactions include both a write and a readback.
  // - The write occurs when set_stb is asserted.
  //   The settings register with the address matching set_addr will
  //   be loaded with the data on set_data.
  // - Readback occurs when rb_stb is asserted. The read back strobe
  //   must assert at least one clock cycle after set_stb asserts /
  //   rb_stb is ignored if asserted on the same clock cycle of set_stb.
  //   Example valid and invalid timing:
  //                 __    __    __    __
  //   clk     __|  |__|  |__|  |__|  |__
  //                    _____
  //   set_stb ___|     |________________
  //                              _____
  //   rb_stb  _________|     |__________     (Valid)
  //                                        _____
  //   rb_stb  _______________|     |____     (Valid)
  //           __________________________
  //   rb_stb                                 (Valid if readback data is a constant)
  //                    _____
  //   rb_stb  ___|     |________________     (Invalid / ignored, same cycle as set_stb)
  //
  localparam [7:0] SR_DELAY = 131;
  localparam [7:0] SR_NB_CHANNEL = 132;
  localparam [7:0] SR_CHANNEL_WIDTH_VAL = 133; 
  localparam [7:0] SR_DETECTED_CHANNEL = 134;
  localparam [7:0] SR_CHANNEL_LEVEL = 135;
  localparam [7:0] SR_GLOBAL_LEVEL = 136;
  localparam [7:0] SR_CHANNEL_WIDTH = 137;


  localparam [7:0] RB_DELAY = 8'd131;
  localparam [7:0] RB_NB_CHANNEL = 8'd132;
  localparam [7:0] RB_CHANNEL_WIDTH_VAL = 8'd133; 
  localparam [7:0] RB_DETECTED_CHANNEL = 8'd134;
  localparam [7:0] RB_CHANNEL_LEVEL = 8'd135;
  localparam [7:0] RB_GLOBAL_LEVEL = 8'd136;
  localparam [7:0] RB_CHANNEL_WIDTH = 8'd137;
////////////////////////////////////////////////////////////
//
// User register
//
////////////////////////////////////////////////////////////
  wire [15:0] delay;
  setting_reg #(
    .my_addr(SR_DELAY), .awidth(8), .width(16))
  sr_delay (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(delay), .changed());

  wire [15:0] nb_channel;
  wire nb_channel_changed;
  setting_reg #(
    .my_addr(SR_NB_CHANNEL), .awidth(8), .width(16))
  sr_nb_channel (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(nb_channel), .changed(nb_channel_changed));

  wire [15:0] channel_width_val;
  setting_reg #(
    .my_addr(SR_CHANNEL_WIDTH_VAL), .awidth(8), .width(16))
  sr_channel_width_val (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(channel_width_val), .changed());


  wire [15:0] detected_channel;
  setting_reg #(
    .my_addr(SR_DETECTED_CHANNEL), .awidth(8), .width(16))
  sr_detected_channel (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(detected_channel), .changed());

  wire [15:0] channel_level;
  setting_reg #(
    .my_addr(SR_CHANNEL_LEVEL), .awidth(8), .width(16))
  sr_channel_level (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(channel_level), .changed());

  wire [15:0] global_level;
  setting_reg #(
    .my_addr(SR_GLOBAL_LEVEL), .awidth(8), .width(16))
  sr_global_level (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(global_level), .changed());

  wire [15:0] channel_width;
  setting_reg #(
    .my_addr(SR_CHANNEL_WIDTH), .awidth(8), .width(16))
  sr_channel_width (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(channel_width), .changed());


////////////////////////////////////////////////////////////
//
// Readback registers
//
////////////////////////////////////////////////////////////
  // rb_stb set to 1'b1 on NoC Shell
  //rb_data is 64bit, delay 16bit so padding of 64-16=48
  always @(posedge ce_clk) begin
    case(rb_addr)
      RB_DETECTED_CHANNEL : rb_data <= {48'd0, detected_channel}; 
      RB_CHANNEL_LEVEL    : rb_data <= {48'd0, channel_level};//min value to reconize a spike as a used channel
      RB_GLOBAL_LEVEL     : rb_data <= {48'd0, global_level};
      RB_CHANNEL_WIDTH    : rb_data <= {48'd0, channel_width};//number of combined bin in output
      RB_DELAY            : rb_data <= {48'd0, delay};
      RB_NB_CHANNEL       : rb_data <= {48'd0, nb_channel}; // number of fft bin
      RB_CHANNEL_WIDTH_VAL: rb_data <= {48'd0, channel_width_val}; 
      default : rb_data <= 64'h0BADC0DE0BADC0DE;
    endcase
  end


////////////////////////////////////////////////////////////
//
// Sample shift register & MAX
//
////////////////////////////////////////////////////////////

  maxi_array #(
    .MAX_NB_CHANNEL(MAX_FFT))
  inst_maxi_array (
    .clk(ce_clk),
    .reset(ce_rst),//| clear_tx_seqnum
    .clear(nb_channel_changed),//nb_channel_changed
    .min_value(channel_level), //nb_channel
    .nb_channel(nb_channel),//nb_channel
    .output_width(channel_width),
    .i_tdata(max_i_tdata),//max_i_tdata[15:0]
    .i_tlast(max_i_tlast),
    .i_tvalid(max_i_tvalid),
    .i_tready(max_i_tready),
    .o_tdata(max_o_tdata),
    .o_tlast(max_o_tlast),
    .o_tvalid(max_o_tvalid),
    .o_tready(max_o_tready));

////////////////////////////////////////////////////////////
//
// Reduce Rate CHDR
//
////////////////////////////////////////////////////////////

// Modify packet header data, same as input except SRC / DST SID fields.
cvita_hdr_modify cvita_hdr_modify (
  .header_in(m_axis_data_tuser),
  .header_out(s_axis_data_tuser),
  .use_pkt_type(1'b0),       .pkt_type(),
  .use_has_time(1'b0),       .has_time(),
  .use_eob(1'b0),            .eob(),
  .use_seqnum(1'b0),         .seqnum(),
  .use_length(1'b0),         .length(),
  .use_payload_length(1'b0), .payload_length(),
  .use_src_sid(1'b1),        .src_sid(src_sid),
  .use_dst_sid(1'b1),        .dst_sid(next_dst_sid),
  .use_vita_time(1'b0),      .vita_time());



////////////////////////////////////////////////////////////
//
// Reduce Rate
//
////////////////////////////////////////////////////////////



// Note: When dropping samples (i.e. vector_mode = 0), unless this block receives a multiple
//       of N packets (where N is the decimation rate), there will be a partial formed output
//       packet. This partial formed packet must be cleared before restarting the block. We
//       can use clear_tx_seqnum for that purpose as it is strobed at block initialization.
keep_one_in_n #(
  .WIDTH(32),
  .MAX_N(MAX_FFT))
keep_one_in_n (
  .clk(ce_clk),
  .reset(ce_rst | clear_tx_seqnum),
  .vector_mode(1'b0), .n(512),//nb_channel//.n(nb_channel>>1),//nb_channel
  .i_tdata(keep_n_i_tdata), .i_tlast(keep_n_i_tlast), .i_tvalid(keep_n_i_tvalid), .i_tready(keep_n_i_tready),
  .o_tdata(keep_n_o_tdata), .o_tlast(keep_n_o_tlast), .o_tvalid(keep_n_o_tvalid), .o_tready(keep_n_o_tready));
    
////////////////////////////////////////////////////////////
//
// Output wiring to axi wrapper
//
////////////////////////////////////////////////////////////
/*
 assign m_axis_data_tready = mag_sq_i_tready;
 assign mag_sq_i_tdata     = m_axis_data_tdata;//{m_axis_data_tdata[15:0],m_axis_data_tdata[15:0]};
 assign mag_sq_i_tlast     = m_axis_data_tlast;
 assign mag_sq_i_tvalid    = m_axis_data_tvalid;

 assign mag_sq_o_tready    = mag_round_i_tready;
 assign mag_round_i_tvalid = mag_sq_o_tvalid;
 assign mag_round_i_tlast  = mag_sq_o_tlast;
 assign mag_round_i_tdata  = mag_sq_o_tdata;    
*/
// assign mag_round_o_tready = keep_n_i_tready;
// assign keep_n_i_tdata = {mag_round_o_tdata, 16'd0};
// assign keep_n_i_tlast = mag_round_o_tlast;
// assign keep_n_i_tvalid = mag_round_o_tvalid;
/*
 assign mag_round_o_tready = max_i_tready;
 assign max_i_tdata        = mag_round_o_tdata;
 assign max_i_tlast        = mag_round_o_tlast;
 assign max_i_tvalid       = mag_round_o_tvalid;
*/
 assign m_axis_data_tready = max_i_tready;
 assign max_i_tdata        = m_axis_data_tdata;
 assign max_i_tlast        = m_axis_data_tlast;
 assign max_i_tvalid       = m_axis_data_tvalid;


assign max_o_tready       = keep_n_i_tready;
assign keep_n_i_tdata     = max_o_tdata;
assign keep_n_i_tlast     = max_o_tlast;
assign keep_n_i_tvalid    = max_o_tvalid;

assign keep_n_o_tready    = s_axis_data_tready;
assign s_axis_data_tdata  = keep_n_o_tdata;
assign s_axis_data_tlast  = keep_n_o_tlast;
assign s_axis_data_tvalid = keep_n_o_tvalid;


/*
 assign max_o_tready          = s_axis_data_tready;
 assign s_axis_data_tdata     = max_o_tdata;
 assign s_axis_data_tlast     = max_o_tlast;
 assign s_axis_data_tvalid    = max_o_tvalid;
*/

endmodule



