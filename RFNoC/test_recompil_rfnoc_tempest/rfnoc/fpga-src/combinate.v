//the last bin witch are not multiple of IN/OUT are discarted

module combinate #(
  parameter VAL = 10,
  parameter BIN_MAX = 100
)(
  input clk, input reset,
  input  [15:0] i_tdata, input i_tlast, input i_tvalid, output i_tready,
  output [15:0] o_tdata, output o_tlast, output o_tvalid, input o_tready
);

  reg [8:0]  bin_counter;  
  reg [8:0]  spp_counter;
  reg [15:0] output_data;
  reg [25:0] temp_data; 
  reg [15:0] last_tdata;
  reg lasted, first;
  localparam max_bin_counter = VAL;//$rtoi($floor(IN/OUT));

  

    always @(posedge clk) begin
      if(i_tlast | reset)begin
        lasted <=1;        
      end

      if (i_tvalid & i_tready & lasted) begin
        lasted <=0;
        first <=1;
        temp_data <=0;
        bin_counter <=0;
        spp_counter <=0;
      end else if (first) begin
        first <=0;
      end

      if (bin_counter == 0) begin
        temp_data =  ((spp_counter%5) == 0) ? i_tdata : i_tdata + last_tdata;
      end else if (bin_counter == max_bin_counter-1) begin
        bin_counter <= 0;
        last_tdata <= i_tdata;
        output_data <= temp_data[18:3];
        spp_counter <= spp_counter+1;        
      end else begin
        temp_data = temp_data+i_tdata;
      end      
      bin_counter = bin_counter+1;
    end
  assign o_tvalid = i_tvalid;
  assign o_tlast  = i_tlast;
  assign o_tdata = (spp_counter <= BIN_MAX) ? output_data : o_tdata;
  assign i_tready = o_tready;


endmodule