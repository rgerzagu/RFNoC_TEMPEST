//first channel is 1, 0 mean no channel detected

module maxi_array #(
  parameter MAX_NB_CHANNEL = 1024
)(
  input clk, input reset, input clear,
  input  [15:0] min_value,
  input  [15:0] nb_channel,
  input  [15:0] output_width,  
  input  [31:0] i_tdata, input i_tlast, input i_tvalid, output i_tready,
  output [31:0] o_tdata, output o_tlast, output o_tvalid, input o_tready
);

  reg [$clog2(MAX_NB_CHANNEL+1)-1:0] pos_max;
  reg [15:0] val_max;
  reg [15:0] pos_max_out;
  reg [15:0] val_max_out;
  reg [15:0] pos_max_out_last;
  reg [15:0] val_max_out_last;
  reg [15:0] energy;
  reg done, ready, switch, run, new, lasted;
  

  generate
    // Counter to track shift register fullness
    reg [15:0] cnt;
    reg [15:0] cnt2;
    reg [15:0] k;

    always @(posedge clk) begin
      if (reset) begin //clear
        cnt <=0;
        cnt2<=0;
        k<=0;
        lasted<=1;
        pos_max<=0;
        val_max<=0;
        pos_max_out<=0;
        pos_max_out_last<=0;
        val_max_out<=0;
        val_max_out_last<=0;
        switch<=1;
        end else begin
        if (i_tvalid & i_tready) begin
          if (cnt < nb_channel) begin //setup counter
            cnt <= cnt + 1;
          end
          if (cnt2 < nb_channel) begin //setup counter
            cnt2 <= cnt2 + 1;            
          end 


          if (lasted) begin
            val_max <= sample_shift_reg0[0];  
            pos_max <= k;
            lasted <=0;
            run<=1;
            k<=1;
            cnt2<=0;
            pos_max_out_last=pos_max;
          end else if (run) begin                  
            if(i_tdata[31:16] > val_max) begin
              pos_max <= k;
              val_max <= i_tdata[31:16];//sample_shift_reg0[0];            
            end   
            k<=k+1;      
          end
          
          if(i_tlast) begin
            lasted<=1;
            run <=0;
            energy<=i_tdata[15:0];
            //pos_max_out <= pos_max+1;
            //val_max_out <= sample_shift_reg0[pos_max-1]+sample_shift_reg0[pos_max]+sample_shift_reg0[pos_max+1];
            //val_max_out <= sample_shift_reg0[pos_max-3]+sample_shift_reg0[pos_max-2]+sample_shift_reg0[pos_max-1]+sample_shift_reg0[pos_max]+sample_shift_reg0[pos_max+1]+sample_shift_reg0[pos_max+2]+sample_shift_reg0[pos_max+3]+sample_shift_reg0[pos_max+4]+sample_shift_reg0[pos_max+5];
            pos_max_out <= (val_max < min_value) ? pos_max_out : pos_max+1;
            val_max_out <= (val_max < min_value) ? val_max_out : val_max; //sample_shift_reg0[pos_max];//val_max;
            //val_max_out_last<=sample_shift_reg0[pos_max+output_width];
            val_max_out_last<=sample_shift_reg0[pos_max_out_last];

            /*if (output_width == 3) begin
              val_max_out <= sample_shift_reg0[pos_max-1] + sample_shift_reg0[pos_max]   + sample_shift_reg0[pos_max+1];
            end else if (output_width == 5) begin
              val_max_out <= sample_shift_reg0[pos_max-2] + sample_shift_reg0[pos_max-1] + sample_shift_reg0[pos_max]   + sample_shift_reg0[pos_max+1] + sample_shift_reg0[pos_max+2];
            end else if (output_width == 7) begin
              val_max_out <= sample_shift_reg0[pos_max-3] + sample_shift_reg0[pos_max-2] + sample_shift_reg0[pos_max-1] + sample_shift_reg0[pos_max]   + sample_shift_reg0[pos_max+1] + sample_shift_reg0[pos_max+2] + sample_shift_reg0[pos_max+3];
            end else if (output_width == 9) begin
              val_max_out <= sample_shift_reg0[pos_max-4] + sample_shift_reg0[pos_max-3] + sample_shift_reg0[pos_max-2] + sample_shift_reg0[pos_max-1] + sample_shift_reg0[pos_max]   + sample_shift_reg0[pos_max+1] + sample_shift_reg0[pos_max+2] + sample_shift_reg0[pos_max+4] + sample_shift_reg0[pos_max+5];
            end else begin
              val_max_out <= sample_shift_reg0[pos_max];
            end   */
            k<=0;
           
          end 
        end//if (i_tvalid & i_tready) 
      end
    end //always @(posedge

    // Sample shift register 
    reg  [15:0] sample_shift_reg0[0:MAX_NB_CHANNEL-1];
    //reg  [15:0] sample_shift_reg1[0:MAX_NB_CHANNEL-1];
    integer n;
    initial begin
      switch <=1;
      for (n = 0; n < MAX_NB_CHANNEL; n = n + 1) begin
        sample_shift_reg0[n] <= 0;
        //sample_shift_reg1[n] <= 0;
      end
    end

  //dual data shift register
   always @(posedge clk) begin
      if (i_tvalid & i_tready) begin
        //if(switch) begin
          for (n = 1; n < MAX_NB_CHANNEL-1; n = n + 1) begin
            sample_shift_reg0[n] <= sample_shift_reg0[n-1];
          end
          sample_shift_reg0[0] <= i_tdata[31:16];
        /*end else begin
          for (n = 1; n < MAX_NB_CHANNEL-1; n = n + 1) begin
            sample_shift_reg1[n] <= sample_shift_reg1[n-1];
          end
          sample_shift_reg1[0] <= i_tdata[31:16];            
        end*/
      end //if (i_tvalid & i_tready) 
    end

    // tlast shift register
  reg [MAX_NB_CHANNEL-1:0] tlast_shift_reg = 0;
  integer m;
  always @(posedge clk) begin
    if (i_tvalid & i_tready) begin
      for (m = 1; m < MAX_NB_CHANNEL; m = m + 1) begin //-1 ?
        tlast_shift_reg[m] <= tlast_shift_reg[m-1];
      end
      tlast_shift_reg[0]   <= i_tlast;
    end
  end

    //assign o_tdata  = (cnt < nb_channel) ? 32'd0 :  //{pos_max_out[9:0], val_max_out[7:0], val_max_out_last[7:0], energy[7:2]}; //pos_max_out,val_max_out
                                      //{re, im}
    assign o_tdata  = (cnt2 < 512) ? {val_max_out, pos_max_out} : {val_max_out_last, energy};  //(nb_channel>>1)) ? {val_max_out, pos_max_out} : {val_max_out_last, energy};

    //assign o_tdata  = {{15'd0,lasted},{15'd0,run}};
    //assign o_tdata  = {val_max[15:0],pos_max[15:0]};

    assign o_tvalid = (cnt < nb_channel) ? 1'b0 : i_tvalid;
    //assign o_tvalid = i_tvalid;
    assign o_tlast  = (cnt < nb_channel) ? 1'b0 : tlast_shift_reg[nb_channel-1];
    //assign o_tlast=tlast_shift_reg[nb_channel-1];
    assign i_tready = o_tready;//done; // & 

    endgenerate
endmodule
