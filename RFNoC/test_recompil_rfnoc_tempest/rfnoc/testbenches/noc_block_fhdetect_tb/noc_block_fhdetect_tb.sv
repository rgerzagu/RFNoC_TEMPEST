`timescale 1ns/1ps
`define NS_PER_TICK 1
`define NUM_TEST_CASES 6

`include "sim_exec_report.vh"
`include "sim_clks_rsts.vh"
`include "sim_rfnoc_lib.svh"

module noc_block_fhdetect_tb();
  `TEST_BENCH_INIT("noc_block_fhdetect",`NUM_TEST_CASES,`NS_PER_TICK);
  localparam BUS_CLK_PERIOD = $ceil(1e9/166.67e6);
  localparam CE_CLK_PERIOD  = $ceil(1e9/200e6);
  localparam NUM_CE         = 1;  // Number of Computation Engines / User RFNoC blocks to simulate
  localparam NUM_STREAMS    = 1;  // Number of test bench streams
  `RFNOC_SIM_INIT(NUM_CE, NUM_STREAMS, BUS_CLK_PERIOD, CE_CLK_PERIOD);
  `RFNOC_ADD_BLOCK(noc_block_fhdetect, 0);

  localparam SPP = 128; // Samples per packet
  localparam TOL = 0.01; // Samples per packet


  localparam IT = 4; // Samples per packet
  integer cnt = 1000;


  /********************************************************
  ** Verification
  ********************************************************/
  initial begin : tb_main
    string s;
    logic [31:0] random_word;
    logic [63:0] readback;
    
    integer value[SPP-1:0];
    for (int i = 0; i < SPP; i+=1) begin        
          value[i]=32'd0;
    end
    // value[0]=32'd0;
    // value[1]=32'd1;
    // value[2]=32'd2;
    // value[3]=32'd16 ;
    //value[4]=32'd200;
    //value[5]=32'd1000;
    value[6]=32'd3000;
    value[7]=32'd5555;
    value[8]=32'd25000;
    value[9]=32'd32766;

    /********************************************************
    ** Test 1 -- Reset
    ********************************************************/
    `TEST_CASE_START("Wait for Reset");
    while (bus_rst) @(posedge bus_clk);
    while (ce_rst) @(posedge ce_clk);
    `TEST_CASE_DONE(~bus_rst & ~ce_rst);

    /********************************************************
    ** Test 2 -- Check for correct NoC IDs
    ********************************************************/
    `TEST_CASE_START("Check NoC ID");
    // Read NOC IDs
    tb_streamer.read_reg(sid_noc_block_fhdetect, RB_NOC_ID, readback);
    $display("Read fhdetect NOC ID: %16x", readback);
    `ASSERT_ERROR(readback == noc_block_fhdetect.NOC_ID, "Incorrect NOC ID");
    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 3 -- Connect RFNoC blocks
    ********************************************************/
    `TEST_CASE_START("Connect RFNoC blocks");
    `RFNOC_CONNECT(noc_block_tb,noc_block_fhdetect,SC16,SPP);
    `RFNOC_CONNECT(noc_block_fhdetect,noc_block_tb,SC16,SPP);
    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 4 -- Write / readback user registers
    ********************************************************/
    `TEST_CASE_START("Write / readback user registers");
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_DELAY, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_DELAY, readback);
    $sformat(s, "User register SR_DELAY incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s);
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_NB_CHANNEL, 16'(SPP) );
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_NB_CHANNEL, readback);
    $sformat(s, "User register SR_NB_CHANNEL incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == 16'(SPP), s);

    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_WIDTH_VAL, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_WIDTH_VAL, readback);
    $sformat(s, "User register SR_CHANNEL_WIDTH_VAL incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s);
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_DETECTED_CHANNEL, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_DETECTED_CHANNEL, readback);
    $sformat(s, "User register SR_DETECTED_CHANNEL incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s);
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_LEVEL, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_LEVEL, readback);
    $sformat(s, "User register SR_CHANNEL_LEVEL incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s);         
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_GLOBAL_LEVEL, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_GLOBAL_LEVEL, readback);
    $sformat(s, "User register SR_GLOBAL_LEVEL incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s); 
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_WIDTH, random_word[15:0]);
    tb_streamer.read_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_WIDTH, readback);
    $sformat(s, "User register SR_CHANNEL_WIDTH incorrect readback! Expected: %0d, Actual %0d", readback[15:0], random_word[15:0]);
    `ASSERT_ERROR(readback[15:0] == random_word[15:0], s);


    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 5 -- Test sequence
    ********************************************************/
    `TEST_CASE_START("Test sequence");
    //  tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_NB_CHANNEL, 16'd3);
    // fork
    //   begin
    //     cvita_payload_t send_payload;        
    //     for (int i = 0; i < SPP/2; i+=1) begin        
    //       send_payload.push_back({16'(value[i]),16'(value[i]),16'(value[i+1]),16'(value[i+1])});
    //       //$display("Value sended! : %D,%D", 32'(value[i]),32'(value[i+1]));
    //     end
    //     tb_streamer.send(send_payload);
    //   end
    //   begin
    //      cvita_payload_t recv_payload;
    //      cvita_metadata_t md;
    //      logic [63:0] expected_value;
    //     tb_streamer.recv(recv_payload,md);
    //     for (int i = 0; i < SPP/2; i+=1) begin
    //          expected_value = {32'(value[i]*value[i]*2),32'(value[i+1]*value[i+1]*2)};
    //       $sformat(s, "Expected: %b Received: %b \n Expected: %b Received: %b ", expected_value[31:0], recv_payload[i][31:0] ,expected_value[63:32]  ,recv_payload[i][63:32] );
    //       $display("%D : Expected: [%d %d] Received: %D", i, expected_value[31:0]*(1-TOL), expected_value[31:0]*(1+TOL) , recv_payload[i][31:0]);
    //       $display("%D : Expected: [%d %d] Received: %D", i, expected_value[63:32]*(1-TOL), expected_value[63:32]*(1+TOL), recv_payload[i][63:32]);
    //       `ASSERT_ERROR(recv_payload[i][63:32] >= expected_value[63:32]*(1-TOL) && recv_payload[i][63:32] <= expected_value[63:32]*(1+TOL) && recv_payload[i][31:0] >= expected_value[31:0] *(1-TOL) && recv_payload[i][31:0] <= expected_value[31:0]*(1+TOL), s);
    //     end
    //   end
    // join
    `TEST_CASE_DONE(1);

// && recv_payload[i][31:0] <= expected_value[31:0]*(1-tol) && recv_payload[i][31:0] >= expected_value[31:0]*(1-tol)

/********************************************************
    ** Test 6 -- Write / readback user registers
    ********************************************************/
    `TEST_CASE_START("Test keep one in N");

      $display("Test N = %0d", SPP);
      tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_NB_CHANNEL, SPP);
      tb_streamer.write_user_reg(sid_noc_block_fhdetect, noc_block_fhdetect.SR_CHANNEL_LEVEL, 200);


      fork
        begin
          // Send N packets, only one packet should come out
          for (int l = 0; l < IT; l++) begin
          //$display   ("---------------------------------------------------------");
            for (int i = 0; i < SPP; i++) begin
              //random_word = $random();

              if (i < 50+3*l) cnt=cnt+1;
              else cnt=cnt-1;
              random_word = {cnt,16'd0};
              //else     random_word = {16'( (SPP-(i * 1*l))),16'd0};



              tb_streamer.push_word(random_word,(i == SPP-1));
              //tb_streamer.push_word(32'(l*SPP+i),(i == SPP-1));
              //$display("%D",32'(l*SPP+i+400));
              //$display("%D",random_word);
                
            end
          end
        end
        begin
          logic [31:0] expected_value, received_value;
          logic tlast;


          for (int l = 0; l < IT-1; l++) begin
            $display   ("---------------------------------------------------------");
            for (int i = 0; i < SPP; i++) begin

              //expected_value = 32'(l*SPP+i-1);
              tb_streamer.pull_word(received_value,tlast);
              //$sformat(s, "N = %0d: Incorrect value received! Expected: %0d, Received: %0d", SPP, expected_value, received_value);
              //$display   ("N = %0d: Value received! Expected: %0d|%0d, Received: %0d|%0d", SPP, expected_value[31:16],expected_value[15:0], received_value[31:16],received_value[15:0]);
              $display   ("N = %0d: Received: %0d|%0d", SPP, received_value[31:16],received_value[15:0]);
              //`ASSERT_ERROR(received_value == expected_value, s);
              if (i == SPP-1) begin
                `ASSERT_ERROR(tlast == 1'b1, "Incorrect packet length! End of packet not asserted!");
              end else begin
                `ASSERT_ERROR(tlast == 1'b0, "Incorrect packet length! End of packet asserted early!");
              end                
            end
          end
        end
      join  
     `TEST_CASE_DONE(1);
    `TEST_BENCH_DONE;

  end
endmodule
