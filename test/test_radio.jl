using RFNoC_TEMPEST

function main()
    # ----------------------------------------------------
    # --- Physical layer parameters
    # ---------------------------------------------------- 
    carrierFreq		= 1200e6;		
    samplingRate	= 100e6; 
    gain			= 3.0; 
    nbSamples		= 1024;
    sFFT            = 1024;

    # ----------------------------------------------------
    # --- Setup radio 
    # ---------------------------------------------------- 
    usrp = openUHD(carrierFreq, samplingRate, gain);
    # ----------------------------------------------------
    # --- Processing
    # ---------------------------------------------------- 
    allIndexes = Array{Int}(undef,0);
    allIQ = Array{Float64}(undef,0);
    try 
        targetDuration = 2;                          # Duration before call to audio device 
        tS        = targetDuration * samplingRate / sFFT;              # Associated power time serie size 
        @info "Press <ctrl-c> to interrupt";
        while (true)
            doBlock = true;
            allP = Array{Float32}(undef, 0);
            while doBlock
                # --- Get buffer
                buffer  = recv(usrp, 2*nbSamples);
                # --- Separate power and index 
                power   = real(buffer);
                indexChannel = Int.(imag(buffer) * 32768);
                # --- Concatenate power in time series 
                push!(allP, power...);
                (length(allP)> tS) && (doBlock = false);
                # --- Concatenate channel detections
                push!(allIndexes,indexChannel...);
                push!(allIQ,power...);
            end
        end
    catch exception 
        # --- Close audio stream 
        close(usrp);
        println(exception);
    end
    return (allIndexes,allIQ);
end
