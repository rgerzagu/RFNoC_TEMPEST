# RFNoC_TEMPEST


A project to use a custom FPGA image 
Be sure to source first the path of UHD 3.15

- RFNoC : Complete archive with source code for FPGA, and UHD driver. Compilation and `make install` gives an updated UHD version to be used with the radio.
- cpp : The CxxWrap part with the glue to link UHD with Julia 
- RFNoC : The minimal Julia UHD wrapper leveraging Cxx 
- test_radio : An example file to configure the radio set the streamer and get the channel indexes and the data
